import React from 'react'
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity} from 'react-native'

const App = () => {
  return (
    <View style={{flex: 1}}>
      <View style={{backgroundColor:'#FFFFFF', flex:1, paddingHorizontal:17, paddingTop:10}}>
        {/* search */}
        <View>
          <View>
            <TextInput placeholder="Apakah anda mencari sesuatu?" style={{backgroundColor: '#e8e8e8', borderWidth: 1, borderColor: '#FFFFFF', height:40, fontSize:13, paddingLeft: 20, paddingRight:28, borderRadius:5}}/>
          </View>
        </View>
        {/* koten1 */}
          <View>
            <View style={{position:'relative', marginTop:10,}}>
              <Image style={{height:145, width:'100%',borderRadius:7,}} source={require('./aset/photo/konten1.jpg')}/>
              {/* <View style={{position:'absolute', width:'100%', height:'100%', backgroundColor:'black', top:0, left:0, opacity:0.2,borderRadius:7}}></View> */}
              <TouchableOpacity style={{position:'absolute',bottom:8, left:0, marginLeft:14}}>
                <Text style={{color:'#ffffff',fontWeight:'bold',fontSize:20}}>APA AJA</Text>
                <Text style={{color:'#ffffff',fontWeight:'400',fontSize:15}}>asal boleh di coba wkwkwk hahaha hihihi huhuhu hohohoho</Text>
              </TouchableOpacity>
            </View>
            <View style={{position:'relative', marginTop:10,}}>
              <Image style={{height:145, width:'100%',borderRadius:7,}} source={require('./aset/photo/konten1.jpg')}/>
              {/* <View style={{position:'absolute', width:'100%', height:'100%', backgroundColor:'black', top:0, left:0, opacity:0.2,borderRadius:7}}></View> */}
              <TouchableOpacity style={{position:'absolute',bottom:8, left:0, marginLeft:14}}>
                <Text style={{color:'#ffffff',fontWeight:'bold',fontSize:20}}>APA AJA</Text>
                <Text style={{color:'#ffffff',fontWeight:'400',fontSize:15}}>asal boleh di coba wkwkwk hahaha hihihi huhuhu hohohoho</Text>
              </TouchableOpacity>
            </View>
          </View>
          {/* hiasan aja */}
          <View style={{ justifyContent:'center', marginTop:1,alignItems:'center'}}>
          <View style={{flexDirection:'row', justifyContent:'center', marginTop:10,alignItems:'center'}}>
            <View style={{height:4, width:100 ,backgroundColor:'#008C00'}}></View>
            <Text style={{fontWeight:'bold', marginHorizontal:7,color:'#008C00'}}>PALENGAAN</Text>
            <View style={{height:4, width:100 ,backgroundColor:'#008C00'}}></View>
          </View>
          <Text style={{color:'#008C00'}}>bersahabat</Text>
          </View>
         {/* menu */}
         <View style={{borderWidth:1, backgroundColor:'#e8e8e8', borderRadius:7, marginTop:10,borderColor:'#008C00'}}>
         <View style={{flexDirection:'row', paddingTop:14, paddingBottom:11}}>
        <View style={{flex:1, alignItems:'center'}}>
          <Image style={{width:30, height:30,}} source={require('./aset/photo/keranjang.png')}/>
          <Text>Belanja</Text>
        </View>
        <View style={{flex:1, alignItems:'center'}}>
          <Image style={{width:35, height:30,}} source={require('./aset/photo/tabungan.png')}/>
          <Text>Tabungan</Text>
        </View>
        <View style={{flex:1, alignItems:'center'}}>
          <Image style={{width:35, height:30,}} source={require('./aset/photo/desa2.png')}/>
          <Text>Profi Desa</Text>
        </View>
        <View style={{flex:1, alignItems:'center'}}>
          <Image style={{width:30, height:30,}} source={require('./aset/photo/dompet.png')}/>
          <Text>Dompet</Text>
        </View>
      </View>
      <View style={{flexDirection:'row', paddingTop:14, paddingBottom:11}}>
        <View style={{flex:1, alignItems:'center'}}>
          <Image style={{width:30, height:30,}} source={require('./aset/photo/kalender.png')}/>
          <Text>Kalender</Text>
        </View>
        <View style={{flex:1, alignItems:'center'}}>
          <Image style={{width:30, height:30,}} source={require('./aset/photo/puskesmas.png')}/>
          <Text>Rumah Sakit</Text>
        </View>
        <View style={{flex:1, alignItems:'center'}}>
          <Image style={{width:30, height:30,}} source={require('./aset/photo/capaian.png')}/>
          <Text>Capaian</Text>
        </View>
        <View style={{flex:1, alignItems:'center'}}>
          <Image style={{width:30, height:30,}} source={require('./aset/photo/menu.png')}/>
          <Text>Lainnya</Text>
        </View>
      </View>

         </View>
     
      </View>

      {/* fother */}
      <View style={{backgroundColor:'#FFFFFF', height:51, flexDirection: 'row'}}>
<View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
  <Image style={{width:24, height:24,}} source={require('./aset/photo/home.png')}/>
<Text style={{fontSize:10, color:'#006D00'}}>Beranda</Text>
</View>
<View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
  <Image style={{width:24, height:24,}} source={require('./aset/photo/berita.png')}/>
<Text style={{fontSize:10}}>Berita</Text>
</View>
<View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
  <Image style={{width:24, height:24,}} source={require('./aset/photo/profil.png')}/>
<Text style={{fontSize:10}}>profil</Text>
</View>
      </View>
    </View>
  )
}

export default App

const styles = StyleSheet.create({})